<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Carbon\Carbon;
use App\raters;
use App\ratingresults;
use App\allbooks;

class ClientController extends Controller {


   public function bookrating()
  {

$getUserSession = raters::where('id', '=', Session::get('rater1'))->first();
if(!$getUserSession)
{
$getUserSession = 'empty';
$allBooks = allbooks::all();
    return view('bookrating')->with('myuser', $getUserSession)->with(compact('allbooks', 'allBooks'));
}

$allBooks = allbooks::all();
    return view('bookrating')->with('myuser', $getUserSession->RaterName)->with(compact('allbooks', 'allBooks'));
  }

public function rateBook(Request $request)
{

$rateBook = new ratingresults();

$getBookName = allbooks::find($request->bookId);

$rateBook->BookId = $request->bookId;

$rateBook->BookName = $getBookName->BookName;

$rateBook->BookRating = $request->rateAssigned;

$rateBook->RatedBy = Session::get('rater1');

$rateBook->save();

return response()->json('done');

}


public function forSignUp(Request $request)
{

$newAim = new raters();

$checkDuplicate = raters::where('RaterName', '=', $request->raterName)->first();
if($checkDuplicate)
{
return response()->json('User Already Exist. Please Login');
}

$newAim->RaterName = $request->raterName;
$newAim->Password =$request->password;
$newAim->save();

return response()->json('done');

}

public function forSignIn(Request $request)
{

$checkDuplicate = raters::where('RaterName', '=', $request->raterName)->where('Password', '=', $request->password)->first();
if($checkDuplicate)
{
Session::put('rater1', $checkDuplicate->id);
return response()->json('valid');
}

return response()->json('invalid');

}

public function forSignOut(Request $request)
{
Session::flush();
return response()->json('loggedOut');
}


public function checkLogin(Request $request)
{

if(Session::get('rater1'))
{

return response()->json('LogedIn');

}

return response()->json('LogedOut');

}

}
