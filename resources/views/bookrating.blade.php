<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
	<title>My Book Rating Site</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
	<!--<link rel="shortcut icon" href="../cssbook/images/favicon.ico" />-->
	<link rel="stylesheet" href="../cssbook/style.css" type="text/css" media="all" />
 <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert-master/dist/sweetalert.css')}}">
	<!--<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<link rel="stylesheet" href="../cssbook/font-awesome-4.7.0/css/font-awesome.min.css">

<style>

</style>
</head>
<body >
	
	<div id="header" class="shell">

		<div id="logo"><h1><a href="#">BookRating</a></h1><span><a href="#">free book rating</a></span></div>
		
		<div id="navigation">
			<ul>
				<li><a href="#" class="active">Home</a></li>
				<li><a href="#">Other Links</a></li>
				
			</ul>
		</div>
		<div class="cl">&nbsp;</div>
		<div id="login-details">
                <?php

			if($myuser != 'empty'){ echo '<p ><span id="isLoggedIn">Welcome,  '.$myuser.'. &emsp; &emsp;&emsp; <a v-on:click="userLogout()" style="margin-left: 30px;font-size: 17px;">Logout</a></span></p>';

}
else{echo '
<p ><span id="notLoggedIn"><a v-on:click="userJoin()" style="font-size: 17px;">Join</a> &emsp; &emsp;&emsp; <a v-on:click="userLogin()" style="margin-left: 30px;font-size: 17px;">Login</a> </span></p>';
//echo '<script>alert("'.$myuser.'");</script>';

}
?>
		</div>
		
	</div>
	
	<div id="slider">
		<div class="shell">
			<ul>
				<li>
					<div class="image">
						<img src="cssbook/images/books.png" alt="" />
					</div>
					<div class="details">
						<h2>1st slide</h2>
						<h3>Best Rating</h3>
						<p class="title">Its a test app for book rating</p>
						<p class="description">Just sign in to be a memeber with access to rate lots of books on our website and get PAID!!!!</p>
						<!--<a href="#" class="read-more-btn">Read More</a>-->
					</div>
				</li>
				<li>
					<div class="image">
						<img src="cssbook/images/books.png" alt="" />
					</div>
					<div class="details">
						<h2>2nd slide</h2>
						<h3>Best Rating </h3>
						<p class="title">Its a test app for book rating</p>
						<p class="description">Just sign in to be a memeber with access to rate lots of books on our website and get PAID!!!!</p>
						<!--<a href="#" class="read-more-btn">Read More</a>-->
					</div>
				</li>
				<li>
					<div class="image">
						<img src="cssbook/images/books.png" alt="" />
					</div>
					<div class="details">
						<h2>3rd slide</h2>
						<h3>Best Rating </h3>
						<p class="title">Its a test app for book rating</p>
						<p class="description">Just sign in to be a memeber with access to rate lots of books on our website and get PAID!!!!</p>
						<!--<a href="#" class="read-more-btn">Read More</a>-->
					</div>
				</li>
				<li>
					<div class="image">
						<img src="cssbook/images/books.png" alt="" />
					</div>
					<div class="details">
						<h2>4th slide</h2>
						<h3>Best Rating </h3>
						<p class="title">Its a test app for book rating</p>
						<p class="description">Just sign in to be a memeber with access to rate lots of books on our website and get PAID!!!!</p>
						<!--<a href="#" class="read-more-btn">Read More</a>-->
					</div>
				</li>
			</ul>
			<div class="nav">
				<a href="#">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">4</a>
			</div>
		</div>
	</div>
	<div id="main" class="shell">
		<div id="sidebar">
			<ul class="categories">
				<li>
					<h4>Categories</h4>
					<ul>
						<li><a href="#">Category1</a></li>
						<li><a href="#">Category2</a></li>
						<li><a href="#">Category3</a></li>
						<li><a href="#">Category4</a></li>
						<li><a href="#">Category5</a></li>
						<li><a href="#">Category6</a></li>
						<li><a href="#">Category7</a></li>
					</ul>
				</li>
				<li>
					<h4>Authors</h4>
					<ul>
						<li><a href="#">SBTDA</a></li>
						<li><a href="#">Author2</a></li>
						<li><a href="#">Author3</a></li>
						<li><a href="#">Author4</a></li>
						<li><a href="#">Author5</a></li>
						<li><a href="#">Author6</a></li>
						<li><a href="#">Author7</a></li>
						<li><a href="#">Author8</a></li>
						<li><a href="#">Author9</a></li>
						<li><a href="#">Author10</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div id="content">
			<div class="products" >
				<h3>Featured Books</h3>
				<ul>
                            <?php foreach(@$allBooks as $eachBook){

                     
                   $ratingInfo = App\ratingresults::where('BookId', '=', $eachBook->id)->get();
                   $countRating = count($ratingInfo);
                   $RatingTotal = $ratingInfo->sum('BookRating');
                    if($countRating > 0){
                   $ratingValue = round($RatingTotal/$countRating);
                    }
                  else{
                    $ratingValue = 0;
                      
                      }
                      $rating = '';
                      switch ($ratingValue) {
case 0:
        $rating = '<span class="description">Average Rating <br><i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;</span>';
        break;
    case 1:
        $rating = '<span class="description">Average Rating <br><i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;</span>';
        break;
    case 2:
        $rating = '<span class="description">Average Rating <br><i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;</span>';
        break;
    case 3:
        $rating = '<span class="description">Average Rating <br><i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star"></i>&nbsp;</span>';
        break;
case 4:
        $rating = '<span class="description">Average Rating <br><i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star" style="color: gold;"></i>&nbsp;<i class="fa fa-star" style="color: gold;"></i>&nbsp;</span>';
        break;
}
					echo '<li>'.
						'<div class="product">'.
							'<a class="info">'.
								'<span class="holder ">'.
									'<div class="readMore1"><img src="cssbook/images/'.$eachBook->BookPath.'" alt="" class="forBookImage" v-on:click="myReadMore(\''.$eachBook->id.'\');" title="Read More"/></div>'.
									'<span class="book-name">'.$eachBook->BookName.'</span>'.
									'<span class="author">by AbdulQawiy</span>'.
									$rating.
								'</span>'.
							'</a>'.
							'<div class="rateHere"><button style="padding-right: 122px;" class="buy-btn" v-on:click="rateNow(\''.$eachBook->id.'\', \''.$eachBook->BookPath.'\')">RATE NOW and get<span class="price"><span class="low">₦</span>1</span></button></div>'.
						'</div>'.
					'</li>';
                                            }
                                          ?>
					
				</ul>
			<!-- End Products -->
			</div>
			<div class="cl">&nbsp;</div>
			<!-- Best-sellers -->
			<div id="best-sellers">
				<h3>Top Ratings</h3>
				<ul>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best01.jpg" alt="" class="forBookImage" v-on:click="myReadMore('9');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best02.jpg" alt="" class="forBookImage" v-on:click="myReadMore('10');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best03.jpg" alt="" class="forBookImage" v-on:click="myReadMore('11');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best04.jpg" alt="" class="forBookImage" v-on:click="myReadMore('12');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best01.jpg" alt="" class="forBookImage" v-on:click="myReadMore('13');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best02.jpg" alt="" class="forBookImage" v-on:click="myReadMore('14');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best03.jpg" alt="" class="forBookImage" v-on:click="myReadMore('15');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
					<li>
						<div class="product">
							<a >
								<div class="readMore1"><img src="cssbook/images/image-best04.jpg" alt="" class="forBookImage" v-on:click="myReadMore('16');" title="Read More"/></div>
								<span class="book-name">Book Name</span>
									<span class="author">by AbdulQawiy</span>
									<span class="description">Book Rating Test Project <br />Coding Challenge</span>
							</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="cl">&nbsp;</div>
	</div>
	<div id="footer" class="shell">
		<div class="top">
			<div class="cnt">
				<div class="col about">
					<h4>About Book Rating</h4>
					<p>This is just a test project </p>
				</div>
				<div class="col store">
					<h4>Store</h4>
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Other Links</a></li>
					</ul>
				</div>
				<!--<div class="col" id="newsletter">
					<h4>Newsletter</h4>
					<p>Lorem ipsum dolor sit amet  consectetur. </p>
					<form action="" method="post">
						<input type="text" class="field" value="Your Name" title="Your Name" />
						<input type="text" class="field" value="Email" title="Email" />
						<div class="form-buttons"><input type="submit" value="Submit" class="submit-btn" /></div>
					</form>
				</div>-->
				<div class="cl">&nbsp;</div>
				<div class="copy">
					<p>&copy; <a href="#">mytestapp.com</a>. Design by <a href="http://mytestapp.com/">Best Rating</a></p>
				</div>
			</div>
		</div>
	</div>




<div class="myModal modalBack none">

</div>

<div class="myModal modalFront none">

<div class="modalStyle">

<div class="modalHead">

    <div align="left" style="display: inline-block;">Please Login</div>
	<div align="right" style="display: inline-block; margin-left: 472px;">
	<i class="fa fa-times" onclick="dismissModal();" style="cursor: pointer;"></i>
	</div>

</div>
<div align="center" style="
    margin-top: 28px;
    /* position: absolute; */
"  >

<div style="
    font-family: cursive;
    font-size: 24px;
">

Sign In

    </div>
<br>
<input type="text" placeholder="Your Email" style="
    width: 324px;
    height: 31px;
    padding: 0px 10px 0px 10px;
">
    <br><br>
    <input type="text" placeholder="Your Password" style="
    width: 324px;
    height: 31px;
    padding: 0px 10px 0px 10px;
">
    <br><br>
    <button type="button" class="toClick" style="
    width: 324px;
    background-color: #F68B1E;
    color: white;
    border: none;
    padding: 8px;
">Login</button>
    
    

</div>

</div>


</div>


<div class="modalLoading none" align="center">
Processing...
</div>



<script type="text/javascript" src="../jsbook/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="../jsbook/jquery.jcarousel.min.js"></script>
	   <script src="{{ asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
		<script type="text/javascript" src="../jsbook/png-fix.js"></script>
	<script src="https://unpkg.com/vue"></script>
	<script type="text/javascript" src="../jsbook/functions.js"></script>
<!--<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->

</body>
</html>