$(function() {
	$('.field, textarea').focus(function() {
        if(this.title==this.value) {
            this.value = '';
        }
    }).blur(function(){
        if(this.value=='') {
            this.value = this.title;
        }
    });

    $('#slider ul').jcarousel({
    	scroll: 1,
		auto: 7,
		itemFirstInCallback : mycarousel_firstCallback,
        wrap: 'both'
    });
   function mycarousel_firstCallback(carousel, item, idx) {
        $('#slider .nav a').bind('click', function() {
            carousel.scroll(jQuery.jcarousel.intval($(this).text()));
            $('#slider .nav a').removeClass('active');
            $(this).addClass('active');
            return false;
        });
        $('#slider .nav a').removeClass('active');
        $('#slider .nav a').eq(idx-1).addClass('active');
    }
	
    $('#best-sellers ul').jcarousel({
        auto: 5,
        scroll: 1,
        wrap: 'circular'
    });
	
     if ($.browser.msie && $.browser.version.substr(0,1)<7) {
        DD_belatedPNG.fix('#logo h1 a, .read-more-btn, #slider .image img, #best-sellers .jcarousel-prev, #best-sellers .jcarousel-next, #slider .jcarousel-container, #best-sellers .price, .shell, #footer, .products ul li a:hover');
    }


});

$(document).mouseup(function(e) 
{
    var container = $('.modalFront');

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.hide();
$('#main').removeClass('fadeBack');
		$('.modalBack').hide();
		$('.modalFront').hide();
$('#footer').removeClass('fadeBack');
$('#slider').removeClass('fadeBack');
$('#header').removeClass('fadeBack');
    }
});

function dismissModal()
{

$('#main').removeClass('fadeBack');
		$('.modalBack').hide();
		$('.modalFront').hide();
$('#footer').removeClass('fadeBack');
$('#slider').removeClass('fadeBack');
$('#header').removeClass('fadeBack');
$('.modalStyle').html('');
    

}

function showModalLoading()
{
$('.modalLoading').show();

$('#main').addClass('fadeBack');
		$('#footer').addClass('fadeBack');
             $('#slider').addClass('fadeBack');
$('#header').addClass('fadeBack');
}

function hideModalLoading()
{
$('.modalLoading').hide();

$('#main').removeClass('fadeBack');
		$('#footer').removeClass('fadeBack');
             $('#slider').removeClass('fadeBack');
$('#header').removeClass('fadeBack');
}

function showModal(myType, bookId, imagePath)
	{
$('.modalStyle').html('');
		
		$('.modalBack').slideDown('fast');
		$('.modalFront').slideDown('fast');
$('#main').addClass('fadeBack');
		$('#footer').addClass('fadeBack');
             $('#slider').addClass('fadeBack');
$('#header').addClass('fadeBack');

if(myType == 'Join'){
$('.modalStyle').html('<div class="modalHead">'+
   '<div align="left" style="display: inline-block;">Fill The Form Below To Join <span style="margin-right: 415px;"></span><i class="fa fa-times" onclick="dismissModal();" style="cursor: pointer;"></i></div>'+
    

'</div>'+
'<div align="center" style="margin-top: 15px;">'+
'<span class="ErrorHere"></span>'+
'<div style="font-family: cursive;font-size: 24px;">'+

'Join Now'+

    '</div>'+
'<br>'+
'<form id="signUpForm">'+
'<input type="text" placeholder="Your Email" id="signUpEmail" name="signUpEmail" style="width: 324px;height: 31px;padding: 0px 10px 0px 10px;">'+
    '<br><br>'+
    '<input type="text" placeholder="Your Password" id="signUpPassword" name="signUpPassword" style="width: 324px;height: 31px;padding: 0px 10px 0px 10px;">'+
    '<br><br>'+
    '<input type="text" placeholder="Your Password Again" id="signUpPasswordAgn" name="signUpPasswordAgn" style="width: 324px;height: 31px;padding: 0px 10px 0px 10px;">'+
    '<br><br>'+
    '<button type="button" class="toClick" onclick="signUpHere();" style="width: 324px;background-color: #F68B1E;color: white;border: none;padding: 8px;">Join</button></form>');
    }
else
if(myType == 'Login'){
$('.modalStyle').html('<div class="modalHead">'+

    '<div align="left" style="display: inline-block;">Please Login</div>'+
	'<div align="right" style="display: inline-block; margin-left: 472px;">'+
	'<i class="fa fa-times" onclick="dismissModal();" style="cursor: pointer;"></i>'+
	'</div>'+

'</div>'+
'<div align="center" style="margin-top: 53px;">'+
'<span class="ErrorHere"></span>'+
'<div style="font-family: cursive;font-size: 24px;">'+

'Sign In'+

    '</div>'+
'<br>'+
'<form id="signInForm"><input type="text" placeholder="Your Email" id="userLoginEmail" name="userLoginEmail" style="width: 324px;height: 31px;padding: 0px 10px 0px 10px;">'+
    '<br><br>'+
    '<input type="text" placeholder="Your Password" id="userLoginPassword" name="userLoginPassword" style="width: 324px;height: 31px;padding: 0px 10px 0px 10px;">'+
    '<br><br>'+
    '<button type="button" onclick="loginUser()" class="toClick" style="width: 324px;background-color: #F68B1E;color: white;border: none;padding: 8px;">Login</button></form>');
    }
else if(myType == 'Rate')
{

$('.modalStyle').html('<div class="modalHead">'+

    '<div align="left" style="display: inline-block;">Rate Now</div>'+
	'<div align="right" style="display: inline-block; margin-left: 472px;">'+
	'<i class="fa fa-times" onclick="dismissModal();" style="cursor: pointer;"></i>'+
	'</div>'+

'</div>'+
'<div align="center" style="margin-top: 24px; width: 100%;">'+
'<div style="color: black;font-weight: bold;font-size: 25px;margin-bottom: 20px;">Select Rating</div>'+
'<div style="display: inline-block; width: 50%;" align="center"><div style="font-family: cursive;font-size: 24px;">'+

'<img src="cssbook/images/'+imagePath+'">'+

    '</div></div>'+

'<div style="display: inline-block; width: 49%;" align="center" id="forModals"><div class="ifSelected" onclick="selectScore(\''+'4'+'\', \''+bookId+'\')">4 &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;</div>'+
    '<br>'+
    '<div class="ifSelected" onclick="selectScore(\''+'3'+'\', \''+bookId+'\')">3 &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gray"></i> &emsp;</div>'+
    '<br>'+
      '<div class="ifSelected" onclick="selectScore(\''+'2'+'\', \''+bookId+'\')">2 &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gray"></i> &emsp;<i class="fa fa-star fa-star-gray"></i> &emsp;</div>'+
    '<br>'+
'<div class="ifSelected" onclick="selectScore(\''+'1'+'\', \''+bookId+'\')">1 &emsp;<i class="fa fa-star fa-star-gold"></i> &emsp;<i class="fa fa-star fa-star-gray"></i> &emsp;<i class="fa fa-star fa-star-gray"></i> &emsp;<i class="fa fa-star fa-star-gray"></i> &emsp;</div></div>'+
    '<br><br><br>'+
    '<!--<button type="button" class="toClick" style="width: 324px;background-color: #F68B1E;color: white;border: none;padding: 8px;">Submit Rating</button>-->');

}
else if(myType == 'More'){

$('.modalStyle').html('<div align="center" style="line-height: 350px;font-size: 35px;">More Book Content Here! Enjoy...</div>');


}
    
	}



var appLogin = new Vue({

el: '#header',
data:{},
methods: {
userLogin(a)
{

showModal('Login', 'N/A', 'N/A');

}, 

userLogout(a)
{

logoutUser();

},
userJoin(a)
{

showModal('Join', 'N/A', 'N/A');

}

}
});



var appRate = new Vue({

el: '#content',
data:{},
methods: {
rateNow(bookId, imagePath)
{

showModalLoading();

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/checkLogin',
        type: 'get',
        data: {'_token': CSRF_TOKEN},
        success: function (data) {
hideModalLoading();
if(data == 'LogedIn'){
  showModal('Rate', bookId,  imagePath);
}
else
{
swal('You Are Not Logged In');
}
        },
        error: function (data) {
hideModalLoading();
            swal("Oops! Error Occured");
        }
    });



}, 

myReadMore(bookId)
{

showModalLoading();
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/checkLogin',
        type: 'get',
        data: {'_token': CSRF_TOKEN},
        success: function (data) {
hideModalLoading();
if(data == 'LogedIn'){
  showModal('More', bookId, 'N/A');
}
else
{
swal('You Are Not Logged In');
}
        },
        error: function (data) {
hideModalLoading();
            swal("Oops! Error Occured");
        }
    });

}
}

});



function selectScore(a, bookId)
{

showModalLoading();
$('.modalFront').hide();
 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/rateBook',
        type: 'get',
        data: {'_token': CSRF_TOKEN, rateAssigned: a, bookId: bookId},
        success: function (data) {
         hideModalLoading();
swal('Rating Saved');
            
        },
        error: function (data) {
hideModalLoading();
            swal("Oops! Error Occured");
        }
    });

}

function loginUser(){

var checkEmpty = [];
var formData = [];
var getAll = $('#signInForm').serializeArray();
$.each(getAll, function(count, val){

if(val.value == "" || val.value == null)
{
checkEmpty.push(val.name);
}


});


if(checkEmpty.length > 0)
{
$('.ErrorHere').html('<span style="color: red;">'+checkEmpty[0]+' Field is required</span>');
return false;
}


if(!validateEmail($('#userLoginEmail').val())){
$('.ErrorHere').html('<span style="color: red;">Invalid Email</span>');
return false;
}

 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/forSignIn',
        type: 'get',
        data: {'_token': CSRF_TOKEN, raterName: $('#userLoginEmail').val(), password: $('#userLoginPassword').val()},
        success: function (data) {
if(data == "invalid")
{
$('.ErrorHere').html('<span style="color: red;">Incorrect Username or Password</span>');
return false;
}
else
{

$('.modalFront').hide();
swal('You are now logged In. Feel free to rate our books');
$('#notLoggedIn').hide();
$('#isLoggedIn').show();
$('#isLoggedIn').html('Welcome, '+$('#userLoginEmail').val());
}
            
        },
        error: function (data) {
$('.modalFront').hide();
            swal("Oops! Error Occured");
        }
    });


}

function signUpHere()
{

var checkEmpty = [];
var formData = [];
var getAll = $('#signUpForm').serializeArray();
$.each(getAll, function(count, val){

if(val.value == "" || val.value == null)
{
checkEmpty.push(val.name);
}
if(val.name == "signUpPassword" || val.name == "signUpPasswordAgn"){

formData.push(val.value);

}

});

if(formData[0] != formData[1])
{
$('.ErrorHere').html('<span style="color: red;">Password Does Not Match</span>');
return false;
}

if(checkEmpty.length > 0)
{
$('.ErrorHere').html('<span style="color: red;">'+checkEmpty[0]+' Field is required</span>');
return false;
}


if(!validateEmail($('#signUpEmail').val())){
$('.ErrorHere').html('<span style="color: red;">Invalid Email</span>');
return false;
}

 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/forSignUp',
        type: 'get',
        data: {'_token': CSRF_TOKEN, raterName: $('#signUpEmail').val(), password: $('#signUpPassword').val(), passwordagn: $('#signUpPasswordAgn').val()},
        success: function (data) {
if(data == "User Already Exist. Please Login")
{
$('.ErrorHere').html('<span style="color: red;">'+data+'</span>');
return false;
}
else
{
$('.ErrorHere').html('<span style="color: green;">SignUp Successful. Please Login.</span>');
return false;

}
            
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });



}

function logoutUser()
{
showModalLoading();
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/forSignOut',
        type: 'get',
        data: {'_token': CSRF_TOKEN},
        success: function (data) {
hideModalLoading();
   swal('You are now logged out!');
$('#notLoggedIn').show();
$('#isLoggedIn').hide();
        },
        error: function (data) {
hideModalLoading();
            swal("Oops! Error Occured");
        }
    });


}

function validateEmail(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};
